# 01 Start from nothing

Okay. Starting from nothing and not knowing all that much about Angular, Angular Material, and TypeScript.

Goal: Get a bare bones Angular project rendering locally.

Okay. Visit <https://angular.io/>. Hit the GET STARTED button.

There's a Stack Blitz thing for trying out Angular in a in-the-cloud development environment. Let's skip to the part where we get to build our own thing.

So <https://angular.io/guide/setup-local>.

Apparently I need NodeJS. Okay.

```shell
$ node -v
v14.19.0
```

Is that a good version? Dunno. Let's try it.

And apparently I need npm (Node Package Manager?). Okay.

```shell
$ npm -v
6.14.16
```

Is that a good version? Still dunno. Let's try it.

(I had these versions already installed from other work. If I didn't, I'd be Googling how to install node, etc.)

```shell
$ npm install -g @angular/cli
...
npm WARN notsup Not compatible with your version of node/npm: @angular-devkit/core@15.0.3
```

I got a bunch of warnings here. Possibly because my Node / NPM aren't recent enough versions. There's like a whole thing with canvas on Apple Silicon.

Well. Let's try moving forward and see what happens.

I made this git repository, cloned it locally, navigated into it, so that I'm doing the ng command line commands inside that repo.

```shell
$ ng new my-app
Node.js version v14.19.0 detected.
The Angular CLI requires a minimum Node.js version of either v14.20, v16.13 or v18.10.

Please update your Node.js version or visit https://nodejs.org/ for additional instructions.
```

Okay. I'm a Homebrew, nvm (Node version manager) kind of person. Let's see if we can use nvm to get to an acceptable NodeJS version.

I don't remember how to do this with nvm offhand, but nvm has pretty good built in help, so here we go.

```shell
$ nvm
...
Example:
  nvm install 8.0.0                     Install a specific version number
  nvm use 8.0                           Use the latest available 8.0.x release
  ...
```

Okay. That's promising. What if I ask it to use the latest 14?

```shell
$ nvm use 14
Now using node v14.19.0 (npm v6.14.16)
```

Nope. 14.19.0 is probably the latest 14 that I have installed. Can I install a later version?

```shell
$ nvm install 14
...
Now using node v14.21.1 (npm v6.14.17)
```

ooh. That's promising, since it's later than `v14.20`.

Let's try that `ng` command line tool again.

```shell
$ ng new hub
ng: command not found
```

Oh, right, I'll need to install again now that I'm using this version of Node.

```shell
npm install -g @angular/cli
/Users/apetro/.nvm/versions/node/v14.21.1/bin/ng -> /Users/apetro/.nvm/versions/node/v14.21.1/lib/node_modules/@angular/cli/bin/ng.js
+ @angular/cli@15.0.3
added 236 packages from 164 contributors in 8.387s
```

That seemed good?

```shell
$ ng new hub
? Would you like to add Angular routing? No
? Which stylesheet format would you like to use? CSS
CREATE hub/README.md (1057 bytes)
...
CREATE hub/src/app/app.component.ts (207 bytes)
✔ Packages installed successfully.
    Directory is already under version control. Skipping initialization of git.
```

Let's try running it.

```shell
cd hub
ng serve --open
```

Okay, something is running. That's promising.

And it's got a bread crumb for what I want to do next, which is install Angular Material.

Next:  [Install Angular Material](./02-install-material.md)

But first, let's commit documentation so far and tag `01-after`.
